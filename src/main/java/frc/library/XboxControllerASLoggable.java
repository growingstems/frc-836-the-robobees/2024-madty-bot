/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import frc.robot.log.LogBuilder;
import java.util.function.Consumer;

public class XboxControllerASLoggable extends XboxControllerLoggable {

    private static final int k_buttonCount = 16;
    private static final int k_axesCount = 6;
    private static final int k_povCount = 1;

    // Logging Consumers
    private final Consumer<Boolean[]> m_logButtons;
    private final Consumer<Double[]> m_logAxes;
    private final Consumer<Integer[]> m_logPovs;

    public XboxControllerASLoggable(int port, String name, LogBuilder builder) {
        super(port, name, builder);

        var entryName = "DS:joystick" + port + "/";

        m_logButtons = builder.makeSyncLogEntry(
                entryName + "buttons", builder.toArray(builder.booleanType), new Boolean[k_buttonCount]);
        m_logAxes = builder.makeSyncLogEntry(
                entryName + "axes", builder.toArray(builder.doubleType), new Double[k_axesCount]);
        m_logPovs = builder.makeSyncLogEntry(
                entryName + "povs", builder.toArray(builder.integerType), new Integer[k_povCount]);
    }

    @Override
    public void updateLog() {
        super.updateLog();
        var buttons = new Boolean[k_buttonCount];
        for (int i = 0; i < k_buttonCount; i++) {
            buttons[i] = getRawButton(i + 1);
        }
        m_logButtons.accept(buttons);

        var axes = new Double[k_axesCount];
        for (int i = 0; i < k_axesCount; i++) {
            axes[i] = getRawAxis(i + 1);
        }
        m_logAxes.accept(axes);

        var povs = new Integer[k_povCount];
        for (int i = 0; i < k_povCount; i++) {
            povs[i] = getPOV(i + 1);
        }
        m_logPovs.accept(povs);
    }
}
