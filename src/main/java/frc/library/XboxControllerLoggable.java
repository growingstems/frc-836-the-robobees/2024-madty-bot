/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj.XboxController;
import frc.robot.log.LogBuilder;
import java.util.function.Consumer;

public class XboxControllerLoggable extends XboxController {
    // Logging Consumers
    private final Consumer<Double> m_leftStickX;
    private final Consumer<Double> m_leftStickY;
    private final Consumer<Double> m_leftTriggerAxis;
    private final Consumer<Double> m_rightStickX;
    private final Consumer<Double> m_rightStickY;
    private final Consumer<Double> m_rightTriggerAxis;
    private final Consumer<Boolean> m_leftBumper;
    private final Consumer<Boolean> m_rightBumper;
    private final Consumer<Boolean> m_aButton;
    private final Consumer<Boolean> m_bButton;
    private final Consumer<Boolean> m_xButton;
    private final Consumer<Boolean> m_yButton;
    private final Consumer<Integer> m_pov;
    private final Consumer<Boolean> m_leftStickButton;
    private final Consumer<Boolean> m_rightStickButton;
    private final Consumer<Boolean> m_startButton;
    private final Consumer<Boolean> m_backButton;

    public XboxControllerLoggable(int port, String name, LogBuilder builder) {
        super(port);
        m_leftStickX = builder.makeSyncLogEntry(name + "/Left Stick X", builder.doubleType, 0.0);
        m_leftStickY = builder.makeSyncLogEntry(name + "/Left Stick Y", builder.doubleType, 0.0);
        m_rightStickX = builder.makeSyncLogEntry(name + "/Right Stick X", builder.doubleType, 0.0);
        m_rightStickY = builder.makeSyncLogEntry(name + "/Right Stick Y", builder.doubleType, 0.0);
        m_leftTriggerAxis =
                builder.makeSyncLogEntry(name + "/Left Trigger Axis", builder.doubleType, 0.0);
        m_rightTriggerAxis =
                builder.makeSyncLogEntry(name + "/Right Trigger Axis", builder.doubleType, 0.0);
        m_leftBumper =
                builder.makeNoRepeatsAsyncEntry(name + "/Left Bumper", builder.booleanType, null);
        m_rightBumper =
                builder.makeNoRepeatsAsyncEntry(name + "/Right Bumper", builder.booleanType, null);
        m_aButton = builder.makeNoRepeatsAsyncEntry(name + "/A Button", builder.booleanType, null);
        m_bButton = builder.makeNoRepeatsAsyncEntry(name + "/B Button", builder.booleanType, null);
        m_xButton = builder.makeNoRepeatsAsyncEntry(name + "/X Button", builder.booleanType, null);
        m_yButton = builder.makeNoRepeatsAsyncEntry(name + "/Y Button", builder.booleanType, null);
        m_pov = builder.makeNoRepeatsAsyncEntry(name + "/POV", builder.integerType, null);
        m_leftStickButton =
                builder.makeNoRepeatsAsyncEntry(name + "/Left Stick Button", builder.booleanType, null);
        m_rightStickButton =
                builder.makeNoRepeatsAsyncEntry(name + "/Right Stick Button", builder.booleanType, null);
        m_startButton =
                builder.makeNoRepeatsAsyncEntry(name + "/Start Button", builder.booleanType, null);
        m_backButton =
                builder.makeNoRepeatsAsyncEntry(name + "/Back Button", builder.booleanType, null);
    }

    public void updateLog() {
        m_leftStickX.accept(getLeftX());
        m_leftStickY.accept(getLeftY());
        m_leftTriggerAxis.accept(getLeftTriggerAxis());
        m_rightStickX.accept(getRightX());
        m_rightStickY.accept(getRightY());
        m_rightTriggerAxis.accept(getRightTriggerAxis());
        m_leftBumper.accept(getLeftBumper());
        m_rightBumper.accept(getRightBumper());
        m_xButton.accept(getXButton());
        m_aButton.accept(getAButton());
        m_bButton.accept(getBButton());
        m_yButton.accept(getYButton());
        m_pov.accept(getPOV());
        m_leftStickButton.accept(getLeftStickButton());
        m_rightStickButton.accept(getRightStickButton());
        m_startButton.accept(getStartButton());
        m_backButton.accept(getBackButton());
    }
}
