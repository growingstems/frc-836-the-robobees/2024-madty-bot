/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.extender;

import org.growingstems.measurements.Measurements.Length;

public class ExtenderSystem {
    private ExtenderI m_extender;

    private static final Length k_stowPosition = Length.ZERO; // TODO find actual stow position
    private static final Length k_acceptableError = Length.ZERO; // TODO find actual acceptable error
    private static final Length k_scorePosition = Length.ZERO; // TODO find actual score position
    private static final Length k_intakeConePosition =
            Length.ZERO; // TODO find actual position to intake cones

    public void goToStowPosition() {
        m_extender.setExtensionLength(k_stowPosition);
    }

    public void goToScorePosition() {
        m_extender.setExtensionLength(k_scorePosition);
    }

    public void goToIntakeConePosition() {
        m_extender.setExtensionLength(k_intakeConePosition);
    }

    public boolean isAtStowPosition() {
        return isAtPosition(k_stowPosition);
    }

    public boolean isAtScorePosition() {
        return isAtPosition(k_scorePosition);
    }

    public boolean isAtIntakeConePosition() {
        return isAtPosition(k_intakeConePosition);
    }

    private boolean isAtPosition(Length position) {
        return getPosition().sub(position).abs().lt(k_acceptableError);
    }

    private Length getPosition() {
        return m_extender.getExtensionLength();
    }
}
