/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.extender;

import org.growingstems.measurements.Measurements.Length;

public class ExtenderDummy implements ExtenderI {

    @Override
    public void stop() {
        // NOP
    }

    @Override
    public void setExtensionLength(Length extension) {
        // NOP
    }

    @Override
    public Length getExtensionLength() {
        return Length.ZERO;
    }
}
