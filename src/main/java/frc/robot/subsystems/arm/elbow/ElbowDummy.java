/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.elbow;

import org.growingstems.measurements.Angle;

public class ElbowDummy implements ElbowI {
    @Override
    public void emergencyStop() {
        // NOP
    }

    @Override
    public void setAngle(Angle angle) {
        // NOP
    }

    @Override
    public Angle getAngle() {
        return Angle.ZERO;
    }
}
