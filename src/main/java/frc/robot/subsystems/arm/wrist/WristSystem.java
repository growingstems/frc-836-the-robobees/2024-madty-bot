/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.wrist;

import org.growingstems.measurements.Angle;

public class WristSystem {
    private WristI m_wrist;

    private static final Angle k_grabCubeAngle = Angle.ZERO; // TODO find grab cube angle
    private static final Angle k_grabConeAngle = Angle.ZERO; // TODO find grab cone angle
    private static final Angle k_stowAngle = Angle.ZERO; // TODO find stow angle
    private static final Angle k_scoreCubeAngle = Angle.ZERO; // TODO find score cube angle
    private static final Angle k_scoreConeAngle = Angle.ZERO; // TODO find score cone angle
    private static final Angle k_acceptableError = Angle.degrees(0); // TODO find acceptable error

    public void goToGrabCubeAngle() {
        m_wrist.setAngle(k_grabCubeAngle);
    }

    public boolean isAtGrabCubeAngle() {
        return isAtAngle(k_grabCubeAngle);
    }

    public void goToGrabConeAngle() {
        m_wrist.setAngle(k_grabConeAngle);
    }

    public boolean isAtGrabConeAngle() {
        return isAtAngle(k_grabConeAngle);
    }

    public void goToStowAngle() {
        m_wrist.setAngle(k_stowAngle);
    }

    public boolean isAtStowAngle() {
        return isAtAngle(k_stowAngle);
    }

    public void goToScoreCubeAngle() {
        m_wrist.setAngle(k_scoreCubeAngle);
    }

    public boolean isAtScoreCubeAngle() {
        return isAtAngle(k_scoreCubeAngle);
    }

    public void goToScoreConeAngle() {
        m_wrist.setAngle(k_scoreConeAngle);
    }

    public boolean isAtScoreConeAngle() {
        return isAtAngle(k_scoreConeAngle);
    }

    private boolean isAtAngle(Angle angle) {
        return getAngle().sub(angle).abs().lt(k_acceptableError);
    }

    private Angle getAngle() {
        return m_wrist.getAngle();
    }
}
