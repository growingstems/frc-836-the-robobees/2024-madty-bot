/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
package frc.robot.subsystems.arm.wrist;

import frc.library.actuators.VelocityActuator;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.AngularVelocity;

public class Wrist implements WristI {
    private static final int k_canId = 0; // TODO find actual canID
    private VelocityActuator<Angle, AngularVelocity> m_tilt;

    @Override
    public void setAngle(Angle angle) {
        m_tilt.setPositionClosedLoop(angle);
    }

    @Override
    public void stop() {
        m_tilt.stop();
    }
}
