/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
package frc.robot.subsystems.arm.hands;

import org.growingstems.measurements.Measurements.Voltage;

public class HandsSystem {
    private HandsI m_hands;

    private static final Voltage k_intakeSpeed = Voltage.ZERO; // TODO find speed
    private static final Voltage k_outtakeSpeed = Voltage.ZERO; // TODO find speed

    public void intake() {
        m_hands.setPower(k_intakeSpeed);
    }

    public void outtake() {
        m_hands.setPower(k_outtakeSpeed);
    }

    public void stop() {
        m_hands.setPower(Voltage.ZERO);
    }
}
