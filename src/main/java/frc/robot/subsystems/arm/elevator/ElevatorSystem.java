/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.elevator;

import org.growingstems.measurements.Measurements.Length;

public class ElevatorSystem {
    private ElevatorI m_elevator;

    private static final Length k_stowPosition = Length.ZERO; // TODO find actual stow position
    private static final Length k_scoreCubePosition =
            Length.ZERO; // TODO find actual score cube position
    private static final Length k_scoreConePosition =
            Length.ZERO; // TODO find actual score cone position
    private static final Length k_acceptableError = Length.ZERO; // TODO find actual acceptable error

    public void goToStowPosition() {
        m_elevator.setPosition(k_stowPosition);
    }

    public void goToScoreCubePosition() {
        m_elevator.setPosition(k_scoreCubePosition);
    }

    public void goToScoreConePosition() {
        m_elevator.setPosition(k_scoreConePosition);
    }

    public boolean isAtStowPosition() {
        return isAtPosition(k_stowPosition);
    }

    public boolean isAtScoreCubePosition() {
        return isAtPosition(k_scoreCubePosition);
    }

    public boolean isAtScoreConePosition() {
        return isAtPosition(k_scoreConePosition);
    }

    private boolean isAtPosition(Length position) {
        return getPosition().sub(position).abs().lt(k_acceptableError);
    }

    private Length getPosition() {
        return m_elevator.getPosition();
    }
}
