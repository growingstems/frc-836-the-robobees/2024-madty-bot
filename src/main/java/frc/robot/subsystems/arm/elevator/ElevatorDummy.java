/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.arm.elevator;

import org.growingstems.measurements.Measurements.Length;

public class ElevatorDummy implements ElevatorI {

    @Override
    public void stop() {
        // NOP
    }

    @Override
    public void setPosition(Length height) {
        // NOP
    }

    @Override
    public Length getPosition() {
        return Length.ZERO;
    }
}
