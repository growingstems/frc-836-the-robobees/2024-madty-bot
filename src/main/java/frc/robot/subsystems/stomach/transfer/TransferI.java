/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.stomach.transfer;

import org.growingstems.measurements.Measurements.Voltage;

public interface TransferI {
    void setPower(Voltage power);

    default void stop() {
        setPower(Voltage.ZERO);
    }

    boolean detectGamePiece();
}
