package frc.robot.subsystems.stomach.transfer;

import org.growingstems.measurements.Measurements.Voltage;

public class TransferSystem {
    private final TransferI m_transfer = new TransferHal();

    private static final Voltage k_sendToHandsPower = Voltage.ZERO; // TODO find speed
    private static final Voltage k_sendToConveyorPower = Voltage.ZERO; // TODO find speed

    public void sendToHands() {
        m_transfer.setPower(k_sendToHandsPower);
    }

    public void sendToConveyor() {
        m_transfer.setPower(k_sendToConveyorPower.neg());
    }

    public void stop() {
        m_transfer.setPower(Voltage.ZERO);
    }

    public boolean transferSensor() {
        return m_transfer.detectGamePiece();
    }
}
