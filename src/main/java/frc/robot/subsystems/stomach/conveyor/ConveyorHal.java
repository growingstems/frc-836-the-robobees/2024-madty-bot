/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.stomach.conveyor;

import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.hardware.TalonFX;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.library.actuators.MotorActuator;
import frc.library.actuators.TalonFxActuator;
import org.growingstems.measurements.Measurements.Voltage;

public class ConveyorHal implements ConveyorI {
    private DigitalInput m_conveyorSensor = new DigitalInput(7);

    private final MotorActuator m_conveyor;
    private static final Voltage k_nominalVoltage = Voltage.volts(12.0);
    private static final int k_canId = 0; // TODO: Find Id

    public ConveyorHal() {
        var motor = new TalonFX(k_canId);
        motor.getConfigurator().apply(new TalonFXConfiguration());

        m_conveyor = TalonFxActuator.motorActuator(motor, k_nominalVoltage);
    }

    @Override
    public void setPower(Voltage power) {
        m_conveyor.setOpenLoop(power.div(k_nominalVoltage).asNone());
    }

    @Override
    public boolean detectGamePiece() {
        return m_conveyorSensor.get();
    }
}
