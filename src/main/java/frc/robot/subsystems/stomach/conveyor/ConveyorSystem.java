/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.stomach.conveyor;

import org.growingstems.measurements.Measurements.Voltage;

public class ConveyorSystem {
    private final ConveyorI m_conveyor = new ConveyorHal();

    private static final Voltage k_transferPower = Voltage.ZERO; // TODO find the necessary power

    public void stop() {
        m_conveyor.setPower(Voltage.ZERO);
    }

    public void toTransfer() {
        m_conveyor.setPower(k_transferPower);
    }

    public boolean conveyorSensor() {
        return m_conveyor.detectGamePiece();
    }
}
