/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.mouth;

import org.growingstems.measurements.Measurements.Voltage;

public class MouthSystem {
    private final MouthI m_mouth = new MouthHal();

    private static final Voltage k_intake = Voltage.ZERO; // TODO find actual power
    private static final Voltage k_outtake = Voltage.ZERO; // TODO find actual power

    public void stop() {
        m_mouth.setPower(Voltage.ZERO);
    }

    public void intake() {
        m_mouth.setPower(k_intake);
    }

    public void outtake() {
        m_mouth.setPower(k_outtake.neg());
    }

    public boolean intakeSensor() {
        return m_mouth.detectGamePiece();
    }
}
