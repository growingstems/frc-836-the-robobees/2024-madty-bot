/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.mouth;

import org.growingstems.measurements.Measurements.Voltage;

public class MouthDummy implements MouthI {

    @Override
    public void setPower(Voltage power) {
        // NOP
    }

    @Override
    public boolean detectGamePiece() {
        return false;
    }
}
