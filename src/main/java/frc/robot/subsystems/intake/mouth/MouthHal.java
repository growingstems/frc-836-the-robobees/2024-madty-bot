/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
package frc.robot.subsystems.intake.mouth;

import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.hardware.TalonFX;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.library.actuators.MotorActuator;
import frc.library.actuators.TalonFxActuator;
import org.growingstems.measurements.Measurements.Voltage;

public class MouthHal implements MouthI {
    private DigitalInput m_intakeSensor = new DigitalInput(5);
    private static final int k_canId = 0; // TODO find actual canID
    private final MotorActuator m_mouth;
    private static final Voltage k_nominalVoltage = Voltage.volts(12.0);

    public MouthHal() {
        var motor = new TalonFX(k_canId);
        motor.getConfigurator().apply(new TalonFXConfiguration());

        m_mouth = TalonFxActuator.motorActuator(motor, k_nominalVoltage);
    }

    @Override
    public void setPower(Voltage power) {
        m_mouth.setOpenLoop(power.div(k_nominalVoltage).asNone());
    }

    @Override
    public boolean detectGamePiece() {
        return m_intakeSensor.get();
    }
}
