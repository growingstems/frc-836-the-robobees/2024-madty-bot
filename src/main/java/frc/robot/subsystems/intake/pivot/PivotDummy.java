/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.pivot;

import org.growingstems.measurements.Angle;

public class PivotDummy implements PivotI {

    @Override
    public void stop() {
        // NOP
    }

    @Override
    public void setAngle(Angle angle) {
        // NOP
    }

    @Override
    public Angle getPosition() {
        return Angle.ZERO;
    }
}
