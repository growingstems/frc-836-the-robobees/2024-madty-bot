/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.pivot;

import org.growingstems.measurements.Angle;

public class PivotSystem {
    private PivotI m_pivot;

    private static final Angle k_stowPosition = Angle.ZERO; // TODO find actual stow position
    private static final Angle k_deployedPosition = Angle.ZERO; // TODO find actual score position
    private static final Angle k_acceptableError = Angle.ZERO; // TODO find actual acceptable error

    public void goToStowPosition() {
        m_pivot.setAngle(k_stowPosition);
    }

    public void goToDeployedPosition() {
        m_pivot.setAngle(k_deployedPosition);
    }

    public boolean isAtStowPosition() {
        return isAtPosition(k_stowPosition);
    }

    public boolean isAtDeployedPosition() {
        return isAtPosition(k_deployedPosition);
    }

    private boolean isAtPosition(Angle position) {
        return getPosition().sub(position).abs().lt(k_acceptableError);
    }

    private Angle getPosition() {
        return m_pivot.getPosition();
    }
}
