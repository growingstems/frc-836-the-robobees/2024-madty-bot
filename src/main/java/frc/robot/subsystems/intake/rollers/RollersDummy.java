/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.rollers;

import org.growingstems.measurements.Measurements.Voltage;

public class RollersDummy implements RollersI {

    @Override
    public void setPower(Voltage power) {
        // NOP
    }
}
