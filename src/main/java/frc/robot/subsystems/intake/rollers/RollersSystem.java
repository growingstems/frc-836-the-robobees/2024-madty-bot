/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake.rollers;

import org.growingstems.measurements.Measurements.Voltage;

public class RollersSystem {
    private RollersI m_rollers;

    private static final Voltage k_intakePower = Voltage.ZERO;
    private static final Voltage k_outtakePower = Voltage.ZERO;
    private static final Voltage k_stopSpinPower = Voltage.ZERO;

    public void intake() {
        m_rollers.setPower(k_intakePower);
    }

    public void outtake() {
        m_rollers.setPower(k_outtakePower.neg());
    }

    public void stop() {
        m_rollers.setPower(k_stopSpinPower);
    }
}
