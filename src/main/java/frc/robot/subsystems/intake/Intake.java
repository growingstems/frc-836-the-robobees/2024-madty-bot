/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.intake.mouth.MouthSystem;
import frc.robot.subsystems.intake.pivot.PivotSystem;
import frc.robot.subsystems.intake.rollers.RollersSystem;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;

public class Intake extends SubsystemBase {
    private RollersSystem m_rollers;
    private PivotSystem m_pivot;
    private MouthSystem m_mouth;
    private Actions m_commandedAction = Actions.STOWING;
    private StateMachine m_sm;

    private EdgeDetector m_fallingEdgeDetector = new EdgeDetector(Edge.FALLING);

    private enum Actions {
        DEPLOYING,
        STOWING,
        INTAKING,
        OUTTAKING,
        STOPPING;
    }

    public Intake() {
        State idle = new InitState("Idle", () -> {
            m_rollers.stop();
            m_pivot.goToStowPosition();
            m_mouth.stop();
        });

        State deployed = new InitState("Deployed", () -> {
            m_rollers.stop();
            m_pivot.goToDeployedPosition();
            m_mouth.stop();
        });

        State intake = new InitState("Intake", () -> {
            m_rollers.intake();
            m_pivot.goToDeployedPosition();
            m_mouth.intake();
        });

        State outtake = new InitState("Outtake", () -> {
            m_rollers.outtake();
            m_pivot.goToDeployedPosition();
            m_mouth.outtake();
        });

        idle.addTransition(new StaticTransition(
                "Deploying",
                deployed,
                () -> m_commandedAction == Actions.DEPLOYING
                        || m_commandedAction == Actions.INTAKING
                        || m_commandedAction == Actions.OUTTAKING));

        deployed.addTransition(
                new StaticTransition("Stowing", idle, () -> m_commandedAction == Actions.STOWING));

        deployed.addTransition(
                new StaticTransition("Intaking", intake, () -> m_commandedAction == Actions.INTAKING));

        deployed.addTransition(
                new StaticTransition("Outtaking", outtake, () -> m_commandedAction == Actions.OUTTAKING));

        intake.addTransition(new StaticTransition(
                "Stopping",
                deployed,
                () -> m_commandedAction == Actions.STOPPING
                        || m_commandedAction == Actions.STOWING
                        || m_fallingEdgeDetector.update(m_mouth.intakeSensor())));

        m_sm = new StateMachine(idle);
    }

    @Override
    public void periodic() {
        m_sm.step();
    }
}
