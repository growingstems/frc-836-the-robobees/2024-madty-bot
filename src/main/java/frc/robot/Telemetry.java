/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.shuffleboard.ComplexWidget;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.FieldObject2d;
import frc.library.utils.UnitEntry;
import org.growingstems.measurements.Measurements.Time;

public class Telemetry {
    // -------------------
    //    General Robot
    // -------------------
    public static class TeleRobot {
        private static final ShuffleboardTab robotTab = Shuffleboard.getTab("Robot");

        public static final GenericEntry badCpuFrames =
                robotTab.add("Bad CPU Frames", 0).withPosition(4, 0).withSize(1, 1).getEntry();

        public static final GenericEntry logFile =
                robotTab.add("Log File", "Unset").withPosition(5, 0).withSize(2, 1).getEntry();

        public static final GenericEntry loggedBytes =
                robotTab.add("Bytes per Frame", 0).withPosition(4, 1).withSize(1, 1).getEntry();

        public static final GenericEntry loggedKBPerSecond = robotTab
                .add("Bytes per Second (kBps)", 0)
                .withPosition(5, 1)
                .withSize(2, 1)
                .getEntry();

        public static final UnitEntry<Time> loopTime = new UnitEntry.Builder<>(
                        robotTab, "Loop Time (ms)", Time.ZERO, Time::milliseconds)
                .withPosition(4, 2)
                .withSize(1, 1)
                .build();

        public static final GenericEntry loggedTotalBytesKB = robotTab
                .add("Total Bytes Logged (kB)", 0)
                .withPosition(5, 2)
                .withSize(2, 1)
                .getEntry();

        public static final GenericEntry noteState =
                robotTab.add("Note State", "UNSET").withPosition(2, 0).withSize(2, 1).getEntry();

        public static final GenericEntry scoreMode =
                robotTab.add("Score State", "UNSET").withPosition(2, 1).withSize(2, 1).getEntry();

        public static final GenericEntry gitCommit =
                robotTab.add("Git Commit", "UNSET").withPosition(7, 0).withSize(2, 1).getEntry();

        public static final GenericEntry gitBranch =
                robotTab.add("Git Branch", "UNSET").withPosition(7, 2).withSize(2, 1).getEntry();
    }

    // ----------------
    //    Drive Team
    // ----------------
    public static class TeleDriveTeam {
        private static ShuffleboardTab driveTeamTab = Shuffleboard.getTab("Drive Team");

        public static final Field2d field = new Field2d();
        public static final FieldObject2d robotPoseMarker = field.getRobotObject();

        @SuppressWarnings("unused")
        private static final ComplexWidget fieldPoseWidget =
                driveTeamTab.add("Field", field).withPosition(5, 0).withSize(7, 4);

        public static final GenericEntry autoLoaded = driveTeamTab
                .add("Auto Loaded", "none")
                .withPosition(1, 0)
                .withSize(4, 1)
                .getEntry();

        public static final UnitEntry<Time> autoStartDelay = new UnitEntry.Builder<>(
                        driveTeamTab, "Auto Start Delay (s)", Time.ZERO, Time::seconds)
                .withPosition(1, 3)
                .withSize(2, 1)
                .build();

        public static final GenericEntry shooterMatchReady =
                driveTeamTab.add("Shooter Pos", false).withPosition(1, 2).withSize(1, 1).getEntry();
        public static final GenericEntry hopperSensorMatchReady = driveTeamTab
                .add("Hopper Sense", false)
                .withPosition(3, 1)
                .withSize(1, 1)
                .getEntry();
        public static final GenericEntry noteCapturedSensorReady = driveTeamTab
                .add("Collect Sense", false)
                .withPosition(4, 1)
                .withSize(1, 1)
                .getEntry();
        public static final GenericEntry ampArmPrimeMatchReady = driveTeamTab
                .add("AmpArm Sense", false)
                .withPosition(3, 2)
                .withSize(1, 1)
                .getEntry();
        public static final GenericEntry shooterPrimeMatchReady = driveTeamTab
                .add("Shooter Sense", false)
                .withPosition(4, 2)
                .withSize(1, 1)
                .getEntry();
    }
}
